/**
 * Created by bolorundurowb on 9/26/2018
 */

const winston = require('winston');
const fs = require('fs');

const logDir = './logs';

if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const tsFormat = function () {
    return (new Date()).toLocaleTimeString();
};

const logger = winston.createLogger({
    transports: [
        new (winston.transports.Console)({
            colorize: true,
            timestamp: tsFormat
        }),
        new (winston.transports.File)({
            filename: `${logDir}/errors.log`,
            timestamp: tsFormat
        })
    ],
    exitOnError: false
});

/**
 * Handles all the logging needs of the app
 */
const _logger = {
    log: (message) => {
        logger.level = 'info';
        logger.info(message);
    },

    error: (err) => {
        logger.level = 'error';
        logger.error(`\n\n${err}`);
    }
};

module.exports = _logger;