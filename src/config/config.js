/**
 * Created by bolorundurowb on 9/26/2018
 */
 
const dotenv = require('dotenv');

const env = process.env.NODE_ENV || 'development';
if (env !== 'production') {
    dotenv.config({ silent: true });
}

const config = {
    secret: process.env.SECRET,
    defaultPassword: process.env.DEFAULT_PASSWORD,
    mongoUrl: process.env.MONGO_URL,
    super: {
        password: process.env.SUPER_PASS,
        username: process.env.SUPER_USERNAME,
        email: process.env.SUPER_EMAIL
    },
    mailgun: {
        user: process.env.MAILGUN_SMTP_LOGIN,
        pass: process.env.MAILGUN_SMTP_PASSWORD
    },
    msg91: {
        senderId: process.env.SENDER_ID,
        routeNo:  process.env.ROUTE_NO,
        apiKey:  process.env.MSG_API_KEY
    },
    googleMapsKey: process.env.MAPS_API_KEY
};

if (env === 'test') {
    config.mongoUrl = process.env.MONGO_TEST_URL;
}


module.exports = config;