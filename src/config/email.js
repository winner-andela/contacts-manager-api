/**
 * Created by bolorundurowb on 9/27/2018
 */
 
 const NodeMailer = require('nodemailer');
 const Logger = require('./logger');
 const Config = require('./config');

 const email = {
     send: (payload) => {
         const credentials = {
             service: 'MailGun',
             auth: {
                 user: Config.mailgun.user,
                 pass: Config.mailgun.pass
             }
         };

         const transport = NodeMailer.createTransport(credentials);

         transport.sendMail(payload, (err) => {
             if (err) {
                 Logger.error(err);
             } else {
                 Logger.log('The email(s) was/were sent successfully.');
             }
         });
     }
 };

 module.exports = email;