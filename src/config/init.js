const mongoose = require('mongoose');

const config = require('./config');
const logger = require('./logger');
const User = require('./../models/user');

mongoose.connect(config.mongoUrl);

User
    .findOne({username: config.super.username})
    .exec((err, user) => {
        if (err) {
            logger.error(err);
            process.exit(1);
        }

        if (user) {
            logger.log('the default super exists.');
            process.exit(0);
        } else {
            user = new User();
            user.username = config.super.username;
            user.email = config.super.email;
            user.password = config.super.password;
            user.tenant = 'default';
            user.role = 'super';
            user.save((err) => {
                if (err) {
                    logger.error(err);
                    process.exit(1);
                } else {
                    logger.log('default super exists.');
                    process.exit(0);
                }
            });
        }
    });