const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');

const routes = require('./routes/routes');
const logger = require('./config/logger');
const config = require('./config/config');

// connect mongoose
mongoose.connect(config.mongoUrl);

// initialize an express object
const server = express();

server.use(cors());

// log requests with morgan
server.use(morgan('dev'));

// parse the payload
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

routes.route(server);

// handle unmatched routes
server.use((req, res, next) => {
    res.status(501).send({ message: 'This API doesn\'t support that function' });
    next();
});

// set the API port
const port = process.env.PORT || 4321;

// start the server
server.listen(port);

// indicate server status
logger.log(`Server started on port: ${port}`);

// expose server to test
module.exports = server;