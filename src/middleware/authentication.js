/**
 * Created by bolorundurowb on 9/26/2018
 */

const jwt = require('jsonwebtoken');

const logger = require('./../config/logger');
const config = require('./../config/config');
const User = require('./../models/user');

const authentication = {
    isAuthenticated: (req, res, next) => {
        const token = req.headers['x-access-token'] || req.headers.token;

        if (token) {
            jwt.verify(token, config.secret, (err, decoded) => {
                if (err) {
                    res.status(401).send({message: 'Failed to authenticate token.'});
                } else {
                    User
                        .findOne({_id: decoded.uid})
                        .exec((err, user) => {
                            if (err) {
                                logger.error(err);
                                res.status(500).send({message: 'An error occurred when retrieving the user'});
                            } else if (!user) {
                                res.status(404).send({message: 'A user with that token no longer exists.'});
                            } else {
                                req.user = user;

                                next();
                            }
                        });
                }
            });
        } else {
            res.status(401).send({message: 'You need to set a token in the request header.'});
        }
    }
};

module.exports = authentication;