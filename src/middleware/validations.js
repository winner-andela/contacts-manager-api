/**
 * Created by bolorundurowb on 9/26/2018
 */
 
 const validations = {
    validateLogin: (req, res, next) => {
        const body = req.body;

        if (!body.username) {
            res.status(400).send({ message: 'A username is required.' });
        } else if (!body.password) {
            res.status(400).send({ message: 'A password is required.' });
        } else {
            next();
        }
    },

    validateCreateUser: (req, res, next) => {
        const body = req.body;

        if (!body.username) {
            res.status(400).send({ message: 'A username is required.' });
        } else {
            next();
        }
    },

    validateCreateAdmin: (req, res, next) => {
        const body = req.body;

        if (!body.username) {
            res.status(400).send({ message: 'A username is required.' });
        } else if (!body.password) {
            res.status(400).send({ message: 'A password is required.' });
        } else if (body.password.length < 5) {
            res.status(400).send({ message: 'A password must be five characters or greater.' });
        } else if (!body.tenantName) {
            res.status(400).send({ message: 'A tenant name is required.' });
        } else {
            next();
        }
    },

    validateCreateContact: (req, res, next) => {
        const body = req.body;

        if (!body.name) {
            res.status(400).send({ message: 'A name is required.' });
        } else if (!body.phone) {
            res.status(400).send({ message: 'A phone number is required.' });
        } else if (body.phone.length !== 11) {
            res.status(400).send({ message: 'The phone number must be 11 characters.' });
        } else if (!body.state) {
            res.status(400).send({ message: 'A state is required.' });
        } else if (!body.country) {
            res.status(400).send({ message: 'A country is required.' });
        } else {
            next();
        }
    },

    validateUserEmail: (req, res, next) => {
        const body = req.body;

        if (!body.title) {
            res.status(400).send({ message: 'A title is required.' });
        } else if (!body.message) {
            res.status(400).send({ message: 'A message is required.' });
        } else {
            next();
        }
    },

    validateUserSms: (req, res, next) => {
        const body = req.body;

        if (!body.message) {
            res.status(400).send({ message: 'A message is required.' });
        } else {
            next();
        }
    }
};

 module.exports = validations;