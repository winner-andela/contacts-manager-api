/**
 * Created by bolorundurowb on 9/26/2018
 */
 
const authorization = {
    isAdmin: (req, res, next) => {
        if (req.user.role === 'admin') {
            next();
        } else {
            res.status(403).send({ message: 'You need to be an admin to access that information' });
        }
    },

    isSuper: (req, res, next) => {
        if (req.user.role === 'super') {
            next();
        } else {
            res.status(403).send({ message: 'You need to be a super to access that information' });
        }
    }
};

module.exports = authorization;