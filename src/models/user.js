const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: {
        type: String
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    passwordHash: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    role: {
        type: String,
        enum: ['user', 'admin', 'super'],
        default: 'user'
    },
    tenant: {
        type: String,
        required: true
    }
});

userSchema.virtual('password').get(function () {
    return this.passwordHash;
});

userSchema.virtual('password').set(function (plainText) {
    const salt = bcrypt.genSaltSync(10);
    this.passwordHash = bcrypt.hashSync(plainText, salt);
});

const userModel = mongoose.model('User', userSchema);

module.exports = userModel;