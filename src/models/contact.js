const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const contactSchema = new Schema ({
    name: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        unique: true
    },
    address: {
        type: String
    },
    ageRange: {
        type: String
    },
    landmark: {
        type: String
    },
    state: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    howTheyFoundUs: {
        type: String
    },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    tenant: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: new Date()
    }
});

const contactModel = mongoose.model('Contact', contactSchema);

module.exports = contactModel;