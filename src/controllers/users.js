/**
 * Created by bolorundurowb on 9/26/2018
 */

const config = require('./../config/config');
const Logger = require('./../config/logger');
const User = require('./../models/user');

const users = {
    create: (req, res) => {
        const body = req.body;
        const caller = req.user;

        User
            .findOne({username: body.username})
            .exec((err, user) => {
                if (err) {
                    Logger.error(err);
                    res.status(500).send({message: 'An error occurred when retrieving users'});
                } else if (user) {
                    res.status(409).send({message: 'A user with that username already exists.'});
                } else {
                    user = new User();
                    user.name = body.name;
                    user.username = body.username;
                    user.password = body.password || config.defaultPassword;
                    user.role = 'user';
                    user.tenant = caller.tenant;
                    user.save((err, _user) => {
                        if (err) {
                            Logger.error(err);
                            res.status(500).send({message: 'An error occurred when retrieving users'});
                        } else {
                            res.status(200).send({
                                id: _user._id,
                                name: user.name,
                                username: user.username,
                                role: user.role
                            });
                        }
                    });
                }
            });
    },

    createAdmin: (req, res) => {
        const body = req.body;

        User
            .findOne({username: body.username})
            .exec((err, user) => {
                if (err) {
                    Logger.error(err);
                    res.status(500).send({message: 'An error occurred when retrieving users'});
                } else if (user) {
                    res.status(409).send({message: 'A user with that username already exists.'});
                } else {
                    user = new User();
                    user.name = body.name;
                    user.username = body.username;
                    user.password = body.password;
                    user.role = 'admin';
                    user.tenant = body.tenantName;
                    user.save((err, _user) => {
                        if (err) {
                            Logger.error(err);
                            res.status(500).send({message: 'An error occurred when retrieving users'});
                        } else {
                            res.status(200).send({
                                id: _user._id,
                                name: user.name,
                                username: user.username,
                                role: user.role
                            });
                        }
                    });
                }
            });
    },

    retrieveUsers: (req, res) => {
        const caller = req. user;

        User
            .find({
                tenant: caller.tenant,
                role: 'user'
            })
            .exec((err, users) => {
                if (err) {
                    Logger.error(err);
                    res.status(500).send({message: 'An error occurred when retrieving users.'});
                } else {
                    res.status(200).send(users);
                }
            });
    },

    retrieveAdmins: (req, res) => {
        User
            .find({ role: 'admin' })
            .exec((err, admins) => {
                if (err) {
                    Logger.error(err);
                    res.status(500).send({message: 'An error occurred when retrieving admins.'});
                } else {
                    res.status(200).send(admins);
                }
            });
    }
};

module.exports = users;