/**
 * Created by bolorundurowb on 9/26/2018
 */

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const config = require('./../config/config');
const Logger = require('./../config/logger');
const User = require('./../models/user');

const auth = {
    login: (req, res) => {
        User
            .findOne({username: req.body.username})
            .exec((err, user) => {
                if (err) {
                    Logger.error(err);
                    res.status(500).send({message: 'An error occurred when retrieving users.'});
                } else if (!user) {
                    res.status(404).send({message: 'A user with that username does not exist.'});
                } else if (verifyPassword(req.body.password, user.passwordHash)) {
                    res.status(200).send({
                        user,
                        token: tokenify(user)
                    });
                } else {
                    res.status(400).send({message: 'The passwords did not match.'});
                }
            });
    },

    resetPassword: (req, res) => {

    }
};

function verifyPassword(plainText, hashedPassword) {
    if (!(plainText && hashedPassword)) {
        return false;
    }

    return bcrypt.compareSync(plainText, hashedPassword);
}

function tokenify(user) {
    return jwt.sign({uid: user._id}, config.secret, {
        expiresIn: '48h'
    });
}

module.exports = auth;