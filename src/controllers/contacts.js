/**
 * Created by bolorundurowb on 9/26/2018
 */

const Logger = require('./../config/logger');
const Config = require('./../config/config');
const Contacts = require('./../models/contact');
const Email = require('./../config/email');
const Sms = require('./../config/sms');
const googleMapsClient = require('@google/maps').createClient({
    key: Config.googleMapsKey
});

const contacts = {
    create: (req, res) => {
        const caller = req.user;
        const body = req.body;

        googleMapsClient.findPlace({
            input: `${body.address}`,
            inputtype: 'textquery'
        }, (err, response) => {
               // had to disable due to quota limitations
        });

        const contact = new Contacts();
        contact.name = body.name;
        contact.phone = body.phone;
        contact.email = body.email;
        contact.address = body.address;
        contact.ageRange = body.ageRange;
        contact.landmark = body.landmark;
        contact.state = body.state;
        contact.country = body.country;
        contact.howTheyFoundUs = body.howTheyFoundUs;
        contact.creator = caller._id;
        contact.tenant = caller.tenant;

        contact.save((err, _contact) => {
            if (err) {
                Logger.error(err);
                res.status(500).send({message: 'An error occurred when saving the contact.'});
            } else {
                res.status(201).send(
                    _contact
                )
            }
        });
    },

    getAll: (req, res) => {
        const caller = req.user;
        const query = {tenant: caller.tenant};
        let limit = 0;
        let offset = 0;

        // add support for querying with limit
        if (req.query.limit) {
            limit = parseInt(req.query.limit, 10);
        }

        // add support for querying with an offset
        if (req.query.skip) {
            offset = parseInt(req.query.skip, 10);
        }

        // filter by search
        const search = req.query.search;
        if (search) {
            query.$or = [
                {
                    name: {
                        $regex: search,
                        $options: 'i'
                    }
                },
                {
                    phone: {
                        $regex: search,
                        $options: 'i'
                    }
                },
                {
                    email: {
                        $regex: search,
                        $options: 'i'
                    }
                },
                {
                    address: {
                        $regex: search,
                        $options: 'i'
                    }
                },
                {
                    ageRange: {
                        $regex: search,
                        $options: 'i'
                    }
                },
                {
                    landmark: {
                        $regex: search,
                        $options: 'i'
                    }
                },
                {
                    state: {
                        $regex: search,
                        $options: 'i'
                    }
                },
                {
                    country: {
                        $regex: search,
                        $options: 'i'
                    }
                }
            ];
        }


        Contacts
            .find(query)
            .sort({createdAt: 'desc'})
            .limit(limit)
            .skip(offset)
            .exec((err, contacts) => {
                if (err) {
                    Logger.error(err);
                    res.status(500).send({message: 'An error occurred when retrieving the contact.'});
                } else {
                    res.status(200).send(contacts);
                }
            });
    },

    getOne: (req, res) => {
        const id = req.params.id;
        const caller = req.user;

        Contacts
            .findOne({_id: id, tenant: caller.tenant})
            .exec((err, contact) => {
                if (err) {
                    Logger.error(err);
                    res.status(500).send({message: 'An error occurred when retrieving the contact.'});
                } else if (!contact) {
                    res.status(404).send({message: 'A contact with that id does not exist.'});
                } else {
                    res.status(200).send(contact);
                }
            });
    },

    update: (req, res) => {

    },

    remove: (req, res) => {

    },

    sendEmail: (req, res) => {
        const id = req.params.id;
        const body = req.body;
        const caller = req.user;

        Contacts
            .findOne({_id: id, tenant: caller.tenant})
            .exec((err, contact) => {
                if (err) {
                    Logger.error(err);
                    res.status(500).send({message: 'An error occurred when retrieving the contact.'});
                } else if (!contact) {
                    res.status(404).send({message: 'A contact with that id does not exist.'});
                } else {
                    if (!contact.email) {
                        res.status(400).send({message: 'The contact does not have an email address.'});
                    } else {
                        Email.send({
                            to: contact.email,
                            from: caller.email || 'no-reply@contact-manager.com',
                            subject: body.title,
                            text: body.message,
                            html: body.message
                        });

                        res.status(200).send({message: 'Email sent successfully.'});
                    }
                }
            });
    },

    sendSms: (req, res) => {
        const id = req.params.id;
        const body = req.body;
        const caller = req.user;

        Contacts
            .findOne({_id: id, tenant: caller.tenant})
            .exec((err, contact) => {
                if (err) {
                    Logger.error(err);
                    res.status(500).send({message: 'An error occurred when retrieving the contact.'});
                } else if (!contact) {
                    res.status(404).send({message: 'A contact with that id does not exist.'});
                } else {
                    if (!contact.phone) {
                        res.status(400).send({message: 'The contact does not have a phone number.'});
                    } else {
                        Sms.send({
                            to: contact.phone,
                            message: body.message
                        });

                        res.status(200).send({message: 'Sms sent successfully.'});
                    }
                }
            });
    }
};

module.exports = contacts;