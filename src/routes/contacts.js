/**
 * Created by bolorundurowb on 9/26/2018
 */

const Authentication = require('./../middleware/authentication');
const Authorization = require('./../middleware/authorization');
const Validations = require('./../middleware/validations');
const ContactsCtrl = require('./../controllers/contacts');

const contact = {
    route: (router) => {
        router.route('/contacts')
            .post(Validations.validateCreateContact, Authentication.isAuthenticated, ContactsCtrl.create)
            .get(Authentication.isAuthenticated, ContactsCtrl.getAll);

        router.route('/contacts/:id')
            .get(Authentication.isAuthenticated, ContactsCtrl.getOne)
            .put(Authentication.isAuthenticated, ContactsCtrl.update)
            .delete(Authentication.isAuthenticated, Authorization.isAdmin, ContactsCtrl.remove);

        router.route('/contacts/:id/email')
            .post(Validations.validateUserEmail, Authentication.isAuthenticated, Authorization.isAdmin, ContactsCtrl.sendEmail);

        router.route('/contacts/:id/sms')
            .post(Validations.validateUserSms, Authentication.isAuthenticated, Authorization.isAdmin, ContactsCtrl.sendSms);
    }
};

module.exports = contact;