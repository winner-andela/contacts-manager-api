/**
 * Created by bolorundurowb on 9/26/2018
 */

const Authentication = require('./../middleware/authentication');
const Validations = require('./../middleware/validations');
const AuthCtrl = require('./../controllers/auth');

const auth = {
    route: (router) => {
        router.route('/login')
            .post(Validations.validateLogin, AuthCtrl.login);

        router.route('/reset-password')
            .post(Authentication.isAuthenticated, AuthCtrl.resetPassword);
    }
};

module.exports = auth;