/**
 * Created by bolorundurowb on 9/26/2018
 */

const authRoutes = require('./auth');
const contactsRoutes = require('./contacts');
const usersRoutes = require('./users');

const routes = {
    route: (router) => {
        authRoutes.route(router);
        contactsRoutes.route(router);
        usersRoutes.route(router);
    }
};

module.exports = routes;