/**
 * Created by bolorundurowb on 9/26/2018
 */

const Authentication = require('./../middleware/authentication');
const Authorization = require('./../middleware/authorization');
const Validations = require('./../middleware/validations');
const UsersCtrl = require('./../controllers/users');

const user = {
    route: (router) => {
        router.route('/users')
            .get(Authentication.isAuthenticated, Authorization.isAdmin, UsersCtrl.retrieveUsers)
            .post(Validations.validateCreateUser, Authentication.isAuthenticated, Authorization.isAdmin, UsersCtrl.create);

        router.route('/users/admin')
            .get(Authentication.isAuthenticated, Authorization.isSuper, UsersCtrl.retrieveAdmins)
            .post(Validations.validateCreateAdmin, Authentication.isAuthenticated, Authorization.isSuper, UsersCtrl.createAdmin);
    }
};

module.exports = user;