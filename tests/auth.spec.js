/**
 * Created by bolorundurowb on 9/28/2018
 */

const supertest = require('supertest');
// eslint-disable-next-line
const should = require('should');
const app = require('../src/server');
const Auth = require('./../src/controllers/auth');

const server = supertest.agent(app);

describe('Auth', () => {
    describe('sign in', () => {
        describe('does not allow', () => {
            it(' for invalid users to be signed in', (done) => {
                server
                    .post('/login')
                    .send({
                        username: 'jane.doe',
                        password: 'john.doe'
                    })
                    .expect(404)
                    .end((err, res) => {
                        res.status.should.equal(404);
                        res.body.message.should.equal('A user with that username does not exist.');
                        done();
                    });
            });

            it(' for users without a username or email to be signed in', (done) => {
                server
                    .post('/login')
                    .send({
                    })
                    .expect(400)
                    .end((err, res) => {
                        res.status.should.equal(400);
                        res.body.message.should.equal('A username is required.');
                        done();
                    });
            });

            it(' for users without password to be signed in', (done) => {
                server
                    .post('/login')
                    .send({
                        username: 'jane.doe'
                    })
                    .expect(400)
                    .end((err, res) => {
                        res.status.should.equal(400);
                        res.body.message.should.equal('A password is required.');
                        done();
                    });
            });

            it(' for users with a wrong password to be signed in', (done) => {
                server
                    .post('/login')
                    .send({
                        username: process.env.SUPER_USERNAME,
                        password: 'john.d'
                    })
                    .expect(400)
                    .end((err, res) => {
                        res.status.should.equal(400);
                        res.body.message.should.equal('The passwords did not match.');
                        done();
                    });
            });
        });

        describe('allows', () => {
            it('for users to be signed in', (done) => {
                server
                    .post('/login')
                    .send({
                        username: process.env.SUPER_USERNAME,
                        password: process.env.SUPER_PASS
                    })
                    .expect(200)
                    .end((err, res) => {
                        res.status.should.equal(200);
                        res.body.should.be.type('object');
                        res.body.should.have.property('token');
                        res.body.token.should.be.type('string');
                        done();
                    });
            });
        });
    });
});